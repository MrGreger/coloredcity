using UnityEngine;
using Zenject;

public class DefaultInstaller : MonoInstaller
{
    [SerializeField]
    private Joystick _flaotingJoystick;

    [SerializeField]
    private GameObject _humanPrefab;
    [SerializeField]
    private GameObject _projectilePrefab;

    [SerializeField]
    private ProjectilePool _projectilePool;

    [SerializeField]
    private HumansDispatcher _humansDispatcher;

    public override void InstallBindings()
    {
        Container.BindInstance(_flaotingJoystick).AsSingle();
        Container.BindInstance<IInputHandler>(GetComponent<JoyStickInputHandler>()).AsSingle();
        Container.BindInstance<ProjectilePool>(_projectilePool).AsSingle();
        Container.BindInstance<HumansDispatcher>(_humansDispatcher).AsSingle();

        Container.BindFactory<Human, Human.HumanFactory>().FromComponentInNewPrefab(_humanPrefab);
        Container.BindFactory<Projectile, Projectile.ProjectileFactory>().FromComponentInNewPrefab(_projectilePrefab);
    }
}