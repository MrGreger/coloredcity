﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[SerializeField]
public class Squad
{
    public int SquadStrength => _squadMembers.Count + (SquadLeader != null ? 1 : 0);

    public IHuman SquadLeader { get; protected set; }
    public Color? Color { get; private set; }

    public ISquadFormatter SquadFormatter { get; protected set; } = new RandomSquadFormatter(3f);

    private List<IHuman> _squadMembers;
    private List<IHumanAI> _humanAIs = new List<IHumanAI>();

    public event System.Action MembersCountChanged;
    public event System.Action<Squad, IHuman> MemverAdded;
    public event System.Action<Squad, IHuman> MemverRemoved;

    [SerializeField]
    private float _stoppingDistance = 1.4f;
    [SerializeField]
    private float _stoppingDistanceDelta = 0f;

    public Squad(IHuman squadLeader, ISquadFormatter squadFormatter, IEnumerable<IHuman> squadMembers) : this()
    {
        AssignLeader(squadLeader);
        SquadFormatter = squadFormatter;

        foreach (var human in squadMembers)
        {
            AddToSquad(human);
        }
    }

    public Squad()
    {
        _squadMembers = new List<IHuman>();
    }

    public void ClearSquad()
    {
        foreach (var human in _squadMembers)
        {
            RemoveFromSquad(human);
        }
    }

    public void AddToSquad(IHuman human)
    {
        if (!CanCaptureHuman(human))
        {
            return;
        }

        human.Squad?.RemoveFromSquad(human);

        _squadMembers.Add(human);
        human.AssignToSquad(this);

        if (human.Transform.TryGetComponent<IHumanAI>(out var humanAI))
        {
            _humanAIs.Add(humanAI);
        }

        MembersCountChanged?.Invoke();
        MemverAdded?.Invoke(this, human);

        _stoppingDistance += _stoppingDistanceDelta;

        for (int i = 0; i < _humanAIs.Count; i++)
        {
            _humanAIs[i].NavMeshAgent.stoppingDistance = _stoppingDistance;

            _humanAIs[i].NavMeshAgent.avoidancePriority = 10;
        }
    }

    public void RemoveFromSquad(IHuman human)
    {
        if (human.Transform.TryGetComponent<IHumanAI>(out var humanAI))
        {
            _humanAIs.Remove(humanAI);
        }
        _squadMembers.Remove(human);

        if (human.IsLeader)
        {
            human.SetLeader(false);
            ReSelectLeader(human);
        }

        _stoppingDistance -= _stoppingDistanceDelta;

        for (int i = 0; i < _humanAIs.Count; i++)
        {
            _humanAIs[i].NavMeshAgent.stoppingDistance = _stoppingDistance;
        }

        human.AssignToSquad(null);
        MembersCountChanged?.Invoke();
        MemverRemoved?.Invoke(this, human);
    }

    private void ReSelectLeader(IHuman oldLeader)
    {
        var wasPlayer = oldLeader.PlayerControlled;

        oldLeader.SetPlayerControlled(false);

        if (_squadMembers.Count > 0)
        {
            var newLeader = _squadMembers.ElementAt(Random.Range(0, _squadMembers.Count));
            _squadMembers.Remove(newLeader);

            if (newLeader.Transform.TryGetComponent<IHumanAI>(out var newLeaderHumanAI))
            {
                _humanAIs.Remove(newLeaderHumanAI);
            }

            newLeader.SetLeader(true);
            newLeader.SetPlayerControlled(wasPlayer);

            newLeader.HumanAI.NavMeshAgent.avoidancePriority = 0;

            AssignLeader(newLeader);
        }
        else
        {
            AssignLeader(null);
        }
    }

    public IEnumerable<IHuman> GetMembers()
    {
        return _squadMembers.AsReadOnly();
    }

    public bool IsMember(IHuman squadMember)
    {
        return _squadMembers.Contains(squadMember);
    }

    public void AssignLeader(IHuman squadLeader)
    {
        if(squadLeader?.SquadColor != null)
        {
            Color = squadLeader.SquadColor;
        }

        SquadLeader = squadLeader;
    }

    public Squad MergeSquads(Squad squadToMerge)
    {
        return new Squad(SquadLeader, SquadFormatter, _squadMembers.Concat(squadToMerge.GetMembers()));
    }

    public void SetFormation(ISquadFormatter squadFormatter)
    {
        SquadFormatter = squadFormatter;
    }

    public void RelocateSquad(Vector3 squadCenter)
    {
        var isRandomFormation = SquadFormatter is RandomSquadFormatter;

        var positions = SquadFormatter.GetSquadPositions(SquadLeader.Transform.position, SquadStrength);

        for (int i = 0; i < _humanAIs.Count; i++)
        {
            if (isRandomFormation)
            {
                _humanAIs[i].SetTarget(positions[i]);
            }
        }
    }

    public void HumanHitted(IHuman human)
    {
        if (human.Squad == this)
        {
            return;
        }

        AddToSquad(human);
    }

    private bool CanCaptureHuman(IHuman human)
    {
        if (human.Squad == null)
        {
            return true;
        }

        return SquadStrength > human.Squad.SquadStrength;
    }
}
