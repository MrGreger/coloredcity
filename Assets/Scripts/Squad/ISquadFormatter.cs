﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISquadFormatter
{
    IList<Vector3> GetSquadPositions(Vector3 center, int membersCount);
}
