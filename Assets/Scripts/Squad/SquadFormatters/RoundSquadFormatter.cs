﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[System.Serializable]
public class RoundSquadFormatter : ISquadFormatter
{
    private float _radius;

    public RoundSquadFormatter(float radius)
    {
        _radius = radius;
    }

    public IList<Vector3> GetSquadPositions(Vector3 center, int membersCount)
    {
        if(membersCount == 0)
        {
            return new List<Vector3>();
        }

        var step = 360 / membersCount ;

        var result = new List<Vector3>();

        for (int i = 0; i < membersCount; i++)
        {
            var position = new Vector3(Mathf.Cos(i * step * Mathf.Deg2Rad), 0, Mathf.Sin(i * step * Mathf.Deg2Rad)) * _radius + center;
            result.Add(position);
        }

        return result;
    }
}

