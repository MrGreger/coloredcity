﻿
using System.Collections.Generic;
using UnityEngine;

public class RandomSquadFormatter : ISquadFormatter
{
    private readonly float _radius = 3;
    private List<Vector3> _positions;

    public RandomSquadFormatter(float radius)
    {
        _radius = radius;
    }

    public IList<Vector3> GetSquadPositions(Vector3 center, int membersCount)
    {
        var result = new List<Vector3>();

        if (_positions == null)
        {
            _positions = new List<Vector3>();
        }

        if (_positions.Count < membersCount)
        {
            _positions.Clear();
            for (int i = 0; i < membersCount; i++)
            {
                AddRandomPoint();
            }
        }
        else
        {
            AddRandomPoint();
        }

        for (int i = 0; i < membersCount; i++)
        {
            var position = center + new Vector3(_positions[i].x, 0, _positions[i].y);
            result.Add(position);
        }

        return result;
    }

    private void AddRandomPoint()
    {
        var randomPoint = Random.insideUnitCircle * _radius;
        _positions.Add(randomPoint);
    }
}

