﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

public class ProjectilePool : SingleObjectPool<Projectile>
{
    private Projectile.ProjectileFactory _projectileFactory;

    [Inject]
    private void Constructor(Projectile.ProjectileFactory projectileFactory)
    {
        _projectileFactory = projectileFactory;
    }

    protected override GameObject CreateObjectForPool(string name = null)
    {
        var newObject = _projectileFactory.Create().gameObject;
        newObject.transform.SetParent(_poolParent);
        newObject.SetActive(false);

        if (name != null)
        {
            newObject.name = name;
        }

        return newObject;
    }
}

