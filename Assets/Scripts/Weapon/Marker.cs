﻿using System;
using UnityEngine;
using Zenject;

public class Marker : MonoBehaviour, IWeapon
{
    public float AttackRange => _attackRange;

    public bool Ready { get; private set; } = true;

    private ProjectilePool _projectilePool;
    private IHuman _owner;

    [SerializeField]
    private float _attackRange = 10;
    [SerializeField]
    private float _coolDown = 2.5f;
    private float _coolDownElapsed = 0;
    private bool _canShoot = false;

    [Inject]
    private void Constructor(ProjectilePool projectilePool)
    {
        _projectilePool = projectilePool;
    }

    public void Shoot()
    {
        if (!_canShoot)
        {
            return;
        }

        _canShoot = false;
        Ready = false;

        var projectile = _projectilePool.GetFromPool(transform.position, transform.rotation, null).GetComponent<Projectile>();
        projectile.transform.position = transform.position;
        projectile.Setup(transform.forward * 500f, _owner);
    }

    public void Shoot(Vector3 direction)
    {
        if (!_canShoot)
        {
            return;
        }

        _canShoot = false;
        Ready = false;

        var projectile = _projectilePool.GetFromPool(transform.position, transform.rotation, null).GetComponent<Projectile>();
        projectile.transform.position = transform.position;
        projectile.transform.rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.LookRotation(direction);
        projectile.Setup(transform.forward * 500f, _owner);
    }

    public void SetOwner(IHuman human)
    {
        _owner = human;
    }

    private void Update()
    {
        if(_coolDownElapsed >= _coolDown)
        {
            _canShoot = true;
            _coolDownElapsed = 0;
            Ready = true;
        }

        _coolDownElapsed += Time.deltaTime;
    }
}

