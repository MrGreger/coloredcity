﻿using Es.InkPainter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{
    public Color Color { get; private set; }
    private Rigidbody _rigidbody;

    private IHuman _shooter;

    private ProjectilePool _projectilePool;

    [SerializeField]
    private Brush _brush;

    private Ray laser;

    [Inject]
    private void Constructor(ProjectilePool projectilePool)
    {
        _rigidbody = GetComponent<Rigidbody>();
        _projectilePool = projectilePool;
    }

    public void Setup(Vector3 velocity, IHuman shooter)
    {
        AssignColor(shooter.SquadColor.Value);

        _rigidbody.isKinematic = false;
        _rigidbody.AddForce(velocity);

        _shooter = shooter;
    }

    private void AssignColor(Color color)
    {
        Color = color;
        GetComponent<MeshRenderer>().material.color = color;
        _brush.Color = color;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_shooter == null)
        {
            return;
        }

        if (collision.gameObject.TryGetComponent<IColorableSurface>(out var surface))
        {
            if (collision.contacts.Length > 0)
            {
                for (int i = 0; i < collision.contacts.Length; i++)
                {
                    surface.Draw(collision.contacts[i].point, _brush);
                }
            }
        }

        DestroyProjectile();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_shooter == null)
        {
            return;
        }

        if (other == null)
        {
            return;
        }

        var otherHuman = other.TryGetComponent<IHuman>(out var human);

        if (!otherHuman)
        {
            otherHuman = other.transform.parent?.TryGetComponent<IHuman>(out human) ?? false;
        }

        if (otherHuman)
        {
            if (human == _shooter || human.Squad == _shooter.Squad)
            {
                return;
            }

            _shooter.Squad.HumanHitted(human);

            DestroyProjectile();
        }

    }

    private void DestroyProjectile()
    {
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector3.zero;
        _shooter = null;
        _projectilePool.ReturnToPool(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(laser);
    }

    public class ProjectileFactory : PlaceholderFactory<Projectile>
    {
    }
}
