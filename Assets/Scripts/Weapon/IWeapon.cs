﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeapon
{
    void SetOwner(IHuman human);
    void Shoot();
    void Shoot(Vector3 direction);
    float AttackRange { get; }
    bool Ready { get; }
}
