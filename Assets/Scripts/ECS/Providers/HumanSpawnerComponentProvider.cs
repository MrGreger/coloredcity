using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Voody.UniLeo;

public sealed class HumanSpawnerComponentProvider : MonoProvider<HumanSpawnerComponent>
{
    private void OnDrawGizmos()
    {
        var oldColor = Gizmos.color;

        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(transform.position, value.Range / 2);

        Gizmos.color = oldColor;
    }
}
