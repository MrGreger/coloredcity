﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ColoredSlider
{
    public Slider Slider { get; }
    public Image Image { get; }

    public ColoredSlider(Slider slider, Image image)
    {
        Slider = slider;
        Image = image;
    }
}

public class ColorScoresTracker : MonoBehaviour
{
    [SerializeField]
    private PaintScore _paintScore;
    [SerializeField]
    private GameObject _sliderPrfab;
    private Dictionary<Color, ColoredSlider> _scoreSliders;

    [Inject]
    private void Constructor()
    {
        if (_paintScore == null)
        {
            return;
        }

        _scoreSliders = new Dictionary<Color, ColoredSlider>();

        _paintScore.ScoresChanged += OnScoresChanged;
    }

    private void OnScoresChanged(Dictionary<Color32, int> scores)
    {
        if (_scoreSliders.Count != scores.Count)
        {
            ResetSliders(scores);
        }

        int allScore = scores.Sum(x => x.Value);

        foreach (var score in scores)
        {
            if (_scoreSliders.TryGetValue(score.Key, out var slider))
            {
                slider.Slider.value = score.Value / (float)allScore;
                slider.Image.color = score.Key;
            }
            else
            {
                ResetSliders(scores);
            }
        }
    }

    private void ResetSliders(Dictionary<Color32, int> scores)
    {
        _scoreSliders.Clear();

        var children = new List<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).TryGetComponent<Slider>(out var s))
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        int allScore = scores.Sum(x => x.Value);

        foreach (var score in scores)
        {
            var sliderGameObject = Instantiate(_sliderPrfab, transform);
            var slider = sliderGameObject.GetComponent<Slider>();

            slider.value = score.Value / (float)allScore;

            var sliderToAdd = new ColoredSlider(slider, slider.fillRect.GetComponent<Image>());

            sliderToAdd.Image.color = score.Key;

            _scoreSliders.Add(score.Key, sliderToAdd);
        }
    }
}

