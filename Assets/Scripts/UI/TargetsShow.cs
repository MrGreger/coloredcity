﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;

public class TargetsShow : MonoBehaviour
{
    public GameObject TargetsContainer;
    public GameObject ImagePrefab;

    private List<Human> _humans;
    private IHuman _player;

    private Camera _mainCamera;

    private Dictionary<Color, RectTransform> _targets = new Dictionary<Color, RectTransform>();

    private HumansDispatcher _humansDispatcher;

    [Inject]
    private void Constructor(HumansDispatcher humansDispatcher)
    {
        _humansDispatcher = humansDispatcher;
    }

    private void Start()
    {

        _humansDispatcher.Squads.ObserveAdd()
                        .Subscribe(x =>
                        {
                            GetHumans();
                        })
                        .AddTo(this);

        _ = _humansDispatcher.Squads.ObserveRemove()
                         .Subscribe(x =>
                         {
                             GetHumans();
                             RemoveMissedSquads();
                         })
                         .AddTo(this);
    }

    private void RemoveMissedSquads()
    {
        foreach (var target in _targets)
        {
            if(!_humans.Any(x=>x.SquadColor == target.Key))
            {
                Destroy(target.Value.gameObject);
            }
        }
    }

    private void OnEnable()
    {
        _mainCamera = Camera.main;
        _humans = GetHumans();
    }

    private void Update()
    {
        var newPlayer = _humansDispatcher.GetPlayer();

        if (newPlayer == null)
        {
            return;
        }

        if (newPlayer != _player)
        {
            _player = newPlayer;
        }

        foreach (var human in _humans)
        {
            if (!_targets.ContainsKey(human.SquadColor.Value))
            {
                var image = Instantiate(ImagePrefab, TargetsContainer.transform);

                var img = image.GetComponent<Image>();
                img.color = human.SquadColor ?? new Color(0, 0, 0, 0);

                _targets.Add(human.SquadColor.Value, image.GetComponent<RectTransform>());
            }

            if (InScreenBound(human.transform))
            {
                _targets[human.SquadColor.Value].gameObject.SetActive(false);
                continue;
            }
            else
            {
                _targets[human.SquadColor.Value].gameObject.SetActive(true);
            }

            RectTransform rectTranform;
            float posY, posX;

            GetTargetPosition(human, out rectTranform, out posY, out posX);

            var anchoredPos = new Vector2(posX, posY);

            rectTranform.anchoredPosition = anchoredPos;
        }
    }

    private void GetTargetPosition(IHuman human, out RectTransform rectTranform, out float posY, out float posX)
    {
        rectTranform = _targets[human.SquadColor.Value];
        var direction = human.Transform.position - _player.Transform.position;
        var direction2D = -new Vector2(direction.x, direction.z);
        direction2D.Normalize();

        rectTranform.rotation = Quaternion.Euler(0, 0, Angle360(Vector2.up, -direction2D));

        var containerRect = TargetsContainer.transform.parent.GetComponent<RectTransform>();

        posY = GetY(rectTranform, direction2D, containerRect);
        posX = GetX(rectTranform, direction2D, containerRect);
    }

    private static float GetX(RectTransform rectTranform, Vector2 direction2D, RectTransform containerRect)
    {
        var posX = 0f;

        if (direction2D.x > 0)
        {
            posX = Mathf.Abs((containerRect.sizeDelta.x / 2) * direction2D.normalized.x);
        }
        else
        {
            posX = -Mathf.Abs(containerRect.sizeDelta.x / 2 * direction2D.normalized.x);
        }

        if (posX + rectTranform.sizeDelta.x / 2 > containerRect.sizeDelta.x / 2)
        {
            posX = containerRect.sizeDelta.x / 2 - rectTranform.sizeDelta.x / 2;
        }
        else if (posX - rectTranform.sizeDelta.x / 2 < -(containerRect.sizeDelta.x / 2))
        {
            posX = -containerRect.sizeDelta.x / 2 + rectTranform.sizeDelta.x / 2;
        }

        return posX;
    }

    private static float GetY(RectTransform rectTranform, Vector2 direction2D, RectTransform containerRect)
    {
        var posY = 0f;

        if (direction2D.y > 0)
        {
            if (1 - direction2D.x <= Mathf.Epsilon || 1 + direction2D.x <= -Mathf.Epsilon)
            {
                posY = (containerRect.sizeDelta.y / 2);
            }
            else
            {
                posY = Mathf.Abs((containerRect.sizeDelta.y / 2) * direction2D.normalized.y);
            }
        }
        else if (direction2D.y < 0)
        {
            if (1 - direction2D.x <= Mathf.Epsilon || 1 + direction2D.x <= -Mathf.Epsilon)
            {
                posY = -(containerRect.sizeDelta.y / 2);
            }
            else
            {
                posY = -Mathf.Abs(containerRect.sizeDelta.y / 2 * direction2D.normalized.y);
            }
        }

        if (posY + rectTranform.sizeDelta.y / 2 > containerRect.sizeDelta.y / 2)
        {
            posY = containerRect.sizeDelta.y / 2 - rectTranform.sizeDelta.y / 2;
        }
        else if (posY - rectTranform.sizeDelta.y / 2 < -(containerRect.sizeDelta.y / 2))
        {
            posY = -containerRect.sizeDelta.y / 2 + rectTranform.sizeDelta.y / 2;
        }

        return posY;
    }

    private List<Human> GetHumans()
    {
        return FindObjectsOfType<Human>().Where(x => x.IsLeader).ToList();
    }

    private float Angle360(Vector2 p1, Vector2 p2, Vector2 o = default(Vector2))
    {
        Vector2 v1, v2;
        if (o == default(Vector2))
        {
            v1 = p1.normalized;
            v2 = p2.normalized;
        }
        else
        {
            v1 = (p1 - o).normalized;
            v2 = (p2 - o).normalized;
        }
        float angle = Vector2.Angle(v1, v2);
        return Mathf.Sign(Vector3.Cross(v1, v2).z) < 0 ? (360 - angle) % 360 : angle;
    }

    private bool InScreenBound(Transform @object)
    {
        var screenPosition = _mainCamera.WorldToViewportPoint(@object.position);

        return (screenPosition.x >= 0 && screenPosition.x <= 1) && (screenPosition.y >= 0 && screenPosition.y <= 1);
    }
}
