﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(MeshRenderer))]
public class HidableObject : MonoBehaviour, IHidable
{
    private MeshRenderer _meshRenderer;
    private Material _material;
    [SerializeField]
    private Material _transparentMaterial;

    [Inject]
    private void Contructor()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _material = _meshRenderer.material;
    }

    public void Hide()
    {
        _meshRenderer.material = _transparentMaterial;
    }

    public void Show()
    {
        _meshRenderer.material = _material;
    }
}

