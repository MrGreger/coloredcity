﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CamerViewHider : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    private void Start()
    {
        StartCoroutine(StartTracking());
    }

    private List<IHidable> _hiddenObjects = new List<IHidable>();
    private List<IHidable> _hiddableTmp = new List<IHidable>();

    private static WaitForSeconds _waitForSeconds = new WaitForSeconds(.1f);

    private RaycastHit[] _hits = new RaycastHit[20];

    private IEnumerator StartTracking()
    {
        while (true)
        {
            if (_target == null)
            {
                yield return null;
            }

            yield return _waitForSeconds;

            var ray = new Ray(transform.position, _target.position - transform.position);

            var obstacles = Physics.RaycastNonAlloc(ray, _hits, float.PositiveInfinity);

            if (obstacles == 0)
            {
                yield return null;
            }

            for (int i = 0; i < obstacles; i++)
            {
                if (_hits[i].transform.TryGetComponent<IHidable>(out var hidable))
                {
                    _hiddableTmp.Add(hidable);
                    _hiddenObjects.Add(hidable);
                    hidable.Hide();
                }
            }

            _hiddenObjects.Where(x => !_hiddableTmp.Contains(x))
                          .ToList()
                          .ForEach(x => x.Show());

            _hiddableTmp.Clear();
        }
    }
}
