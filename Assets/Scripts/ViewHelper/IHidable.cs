﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHidable 
{
    void Hide();
    void Show();
}
