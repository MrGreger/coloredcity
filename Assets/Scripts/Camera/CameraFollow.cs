﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraFollow : MonoBehaviour
{

    [SerializeField]
    private Transform _target;

    private IHuman _player;

    [SerializeField]
    private float _speed;

    private Vector3? _offset;

    private HumansDispatcher _humansDispatcher;

    [Inject]
    private void Constructor(HumansDispatcher humansDispatcher)
    {
        _humansDispatcher = humansDispatcher;
    }

    private void LateUpdate()
    {
        var newPlayer = _humansDispatcher.GetPlayer();

        if (newPlayer == null)
        {
            return;
        }

        if (newPlayer != _player)
        {
            _player = newPlayer;

            _target = _player.Transform;

            if (_offset == null)
            {
                _offset = transform.position - _target.transform.position;
            }
        }

        if (_target == null)
        {
            return;
        }

        var desiredPosition = _offset.Value + _target.position;
        transform.position = Vector3.Lerp(transform.position, desiredPosition, _speed * Time.deltaTime);

        var desiredRotation = Quaternion.LookRotation(_target.position - transform.position);
        desiredRotation.eulerAngles = new Vector3(desiredRotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);

        transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, _speed * Time.deltaTime);
    }
}
