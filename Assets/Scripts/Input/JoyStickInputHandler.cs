﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class JoyStickInputHandler : MonoBehaviour, IInputHandler
{
    private Joystick _fixedJoystick;

    [Inject]
    public void Constructor(Joystick fixedJoystick)
    {
        _fixedJoystick = fixedJoystick;
    }

    public Vector2 GetInput()
    {
        return _fixedJoystick.Direction;
    }
}
