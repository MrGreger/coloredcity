﻿using Es.InkPainter;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class Painter : MonoBehaviour, IColorableSurface
{
    public int Resolution = 3072;

    private Vector2Int _actualResolution = Vector2Int.zero;
    private Vector2Int ActualResolution => _actualResolution;

    private Texture _paintTexture;
    private Material _coloringMaterial;

    private InkCanvas _canvas;

    public Texture PaintTexture => _paintTexture;

    private void Start()
    {
        _canvas = GetComponent<InkCanvas>();
    }

    [Inject]
    private void Constructor()
    {
        AssignTexture();
    }

    public void SetResolution(int resolution)
    {
        //add validations 
        Resolution = resolution;
        AssignTexture();
    }

    private void AssignTexture()
    {
        CalculateResolution(Resolution);

        _coloringMaterial = GetComponent<Renderer>().material;

        _paintTexture = CreateClearTexture();
        _coloringMaterial.SetTexture("_MainTex", _paintTexture);
    }

    private void CalculateResolution(int resolution)
    {
        if (transform.localScale != Vector3.one)
        {
            if (transform.localScale.x > transform.localScale.z)
            {

                _actualResolution = new Vector2Int(resolution, Mathf.RoundToInt(resolution * transform.localScale.z / transform.localScale.x));
            }
            else
            {
                _actualResolution = new Vector2Int(Mathf.RoundToInt(resolution * transform.localScale.x / transform.localScale.z), resolution);
            }
        }
        else
        {
            _actualResolution = new Vector2Int(resolution, resolution);
        }
    }

    private RenderTexture GetTexture(Texture2D sourceTexture)
    {
        RenderTexture rt = new RenderTexture(_actualResolution.x, _actualResolution.y, 1, RenderTextureFormat.ARGB32);
        Graphics.Blit(sourceTexture, rt);
        return rt;
    }

    private Texture2D CreateClearTexture()
    {
        var clearMap = TextureHelper.CreateTexture2D(_actualResolution.x, _actualResolution.y);
        clearMap.Apply();

        return clearMap;
    }

    public void Draw(RaycastHit hitInfo, Brush paintBrush)
    {
        _canvas.Paint(paintBrush, hitInfo);
    }

    public void Draw(Vector3 position, Brush paintBrush)
    {
        _canvas.PaintNearestTriangleSurface(paintBrush, position);
    }

}