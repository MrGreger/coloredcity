﻿
using System.Linq;
using UnityEngine;
public class PaintBrush
{
    public Color Color { get; set; }
    public int Size { get; set; }
    public Texture2D Texture { get; set; }
}

