﻿using Es.InkPainter;
using System.Collections.Generic;
using UnityEngine;

public interface IColorableSurface
{
    void Draw(RaycastHit hitInfo, Brush paintBrush);
    void Draw(Vector3 position, Brush paintBrush);
}

