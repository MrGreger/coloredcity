﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UniRx;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Rendering;
using Zenject;
using Debug = UnityEngine.Debug;

public class PaintScore : MonoBehaviour
{
    public event Action<Dictionary<Color32, int>> ScoresChanged;

    [SerializeField]
    private int _chunkSize = 10;

    public MeshRenderer Renderer;

    private Texture _mainTexture => Renderer.material.mainTexture;

    private HumansDispatcher _humansDispatcher;

    private HashSet<Color32> _humanColors;

    private WaitForEndOfFrame _waitForEndOfFrame;
    private WaitForSeconds _scoreDelay = new WaitForSeconds(2);

    private NativeHashMap<Color, int> _resultsBuffer;

    [Inject]
    private void Constructor(HumansDispatcher humansDispatcher)
    {
        _humansDispatcher = humansDispatcher;
    }

    private NativeArray<byte> _colorBuffer;
    private void Awake()
    {
        _colorBuffer = new NativeArray<byte>(0, Allocator.Persistent);
        _resultsBuffer = new NativeHashMap<Color, int>(100, Allocator.Persistent);
    }

    private void Start()
    {

        _humanColors = new HashSet<Color32>(_humansDispatcher.Squads.Where(x => x.Color.HasValue).Select(x => (Color32)x.Color.Value));

        _humansDispatcher.Squads.ObserveAdd()
                                .Subscribe(x =>
                                {
                                    if (x.Value.Color.HasValue)
                                    {
                                        _humanColors.Add(x.Value.Color.Value);
                                    }
                                })
                                .AddTo(this);

        _humansDispatcher.Squads.ObserveRemove()
                                .Subscribe(x =>
                                {
                                    //if (x.Value.Color.HasValue)
                                    //{
                                    //    _humanColors.Remove(x.Value.Color.Value);
                                    //}
                                })
                                .AddTo(this);
        StartCoroutine(CountScores());

    }

    private void OnDisable()
    {
        _resultsBuffer.Dispose();
        _colorBuffer.Dispose();
    }

    Dictionary<Color32, int> _colorScores = new Dictionary<Color32, int>();
    Dictionary<Color32, int> _eventBuffer = new Dictionary<Color32, int>();
    ColorComparer comparer = new ColorComparer(0);
    private IEnumerator CountScores()
    {
        if (_mainTexture == null)
        {
            yield break;
        }

        while (true)
        {

            if (!_humanColors.Any())
            {
                yield return null;
                continue;
            }

            yield return CountScoresForTexture(_mainTexture);

            _eventBuffer.Clear();

            for (var i = 0; i < _colorScores.Keys.Count; i++)
            {
                var colorGroupKey = _colorScores.Keys.ElementAt(i);

                if (!_humanColors.Contains(colorGroupKey, comparer))
                {
                    continue;
                }

                if (_colorScores[colorGroupKey] < 2)
                {
                    continue;
                }

                _eventBuffer.Add(colorGroupKey, _colorScores[colorGroupKey]);

                if (i % 10 == 0)
                {
                    yield return _waitForEndOfFrame;
                }
            }

            ScoresChanged?.Invoke(_eventBuffer);

            _colorScores.Clear();

            yield return _scoreDelay;
        }
    }

    private IEnumerator CountScoresForTexture(Texture texture)
    {
        var request = AsyncGPUReadback.Request(texture);

        while (!request.done)
        {
            yield return _waitForEndOfFrame;
        }

        var buffer = request.GetData<byte>();
        if (buffer.Length != _colorBuffer.Length)
        {
            _colorBuffer = new NativeArray<byte>(buffer.Length, Allocator.Persistent);
        }

        buffer.CopyTo(_colorBuffer);

        _resultsBuffer.Clear();
        var scoreJob = new ColorScoringJob
        {
            ColorData = _colorBuffer,
            Result = _resultsBuffer
        };

        var scoreHandle = scoreJob.Schedule();

        while (!scoreHandle.IsCompleted)
        {
            yield return _waitForEndOfFrame;
        }

        scoreHandle.Complete();

        if (_colorScores == null)
        {
            _colorScores = new Dictionary<Color32, int>();
        }
        else
        {
            _colorScores.Clear();
        }

        var keys = scoreJob.Result.GetKeyValueArrays(Allocator.TempJob);

        for (int i = 0; i < keys.Length; i++)
        {
            _colorScores.Add(keys.Keys[i], scoreJob.Result[keys.Keys[i]]);

            if (i % 10 == 0)
            {
                yield return _waitForEndOfFrame;
            }
        }
    }
}

public class ColorComparer : IEqualityComparer<Color32>
{
    private int _threshold;

    public ColorComparer(int threshold)
    {
        _threshold = threshold;
    }

    public static bool BasicEquals(Color32 c1, Color32 c2)
    {
        return c1.r == c2.r && c1.g == c2.g && c1.b == c2.b && c1.a == c2.a;
    }

    public bool Equals(Color32 x, Color32 y)
    {
        var result = Mathf.Abs(x.r - y.r) <= _threshold && Mathf.Abs(x.g - y.g) <= _threshold && Mathf.Abs(x.b - y.b) <= _threshold;

        if (!result)
        {

        }

        return result;
    }

    public int GetHashCode(Color32 obj)
    {
        return obj.GetHashCode();
    }
}

[BurstCompile]
public struct ColorScoringJob : IJob
{
    public NativeArray<byte> ColorData;
    public NativeHashMap<Color, int> Result;

    public void Execute()
    {
        for (int j = 0; j < ColorData.Length; j += 4)
        {
            var color = new Color32(ColorData[j], ColorData[j + 1], ColorData[j + 2], ColorData[j + 3]);
            if (Result.ContainsKey(color))
            {
                Result[color]++;
            }
            else
            {
                Result.Add(color, 1);
            }
        }
    }
}