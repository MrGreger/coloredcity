﻿using System.Collections.Generic;
using UnityEngine;

public class SingleObjectPool<T> : MonoBehaviour
{
    [SerializeField]
    protected GameObject _objectPrefab;
    [SerializeField]
    protected Transform _poolParent;
    [SerializeField]
    protected int _poolObjectsCount;
    protected IList<GameObject> _pool;

    private bool _initialized = false;
    public bool Initialized => _initialized;

    private void Start()
    {
        Initialize();
    }

    public virtual GameObject GetFromPool(Vector3 position, Quaternion rotation, Transform parent)
    {
        GameObject notActiveObject = null;

        for (int i = 0; i < _pool.Count; i++)
        {
            if(!_pool[i].activeInHierarchy)
            {
                notActiveObject = _pool[i];
                break;
            }
        }

        if (notActiveObject == null)
        {
            var toAdd = CreateObjectForPool("poolObject-" + _pool.Count.ToString());
            _pool.Add(toAdd);
            notActiveObject = toAdd;
        }

        notActiveObject.transform.parent = parent;
        notActiveObject.transform.position = position;
        notActiveObject.transform.rotation = rotation;
        notActiveObject.transform.localScale = _objectPrefab.transform.localScale;
        notActiveObject.SetActive(true);

        return notActiveObject;
    }

    public virtual void ReturnToPool(GameObject gameObject)
    {
        gameObject.SetActive(false);
        gameObject.transform.parent = _poolParent;
        gameObject.transform.position = Vector3.zero;
        gameObject.transform.rotation = Quaternion.identity;
    }

    public virtual void ReturnAllToPool()
    {
        if(_pool == null)
        {
            return;
        }

        foreach (var @object in _pool)
        {
            ReturnToPool(@object);
        }
    }

    protected virtual void CreatePool()
    {
        _pool = new List<GameObject>();

        for (int i = 0; i < _poolObjectsCount; i++)
        {
            _pool.Add(CreateObjectForPool($"poolObject-{i}"));
        }
    }

    protected virtual GameObject CreateObjectForPool(string name = null)
    {
        var newObject = Instantiate(_objectPrefab, _poolParent);
        newObject.SetActive(false);

        if (name != null)
        {
            newObject.name = name;
        }

        return newObject;
    }

    public void Initialize()
    {
        CreatePool();
        _initialized = true;
    }
}

