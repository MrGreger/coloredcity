﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoolObjectDescrtion<T>
{
    public T ObjectName;
    public int PoolSize;
    public GameObject Prefab;

    public PoolObjectDescrtion(T objectName, int poolSize, GameObject prefab)
    {
        ObjectName = objectName;
        PoolSize = poolSize;
        Prefab = prefab;
    }
}

public abstract class MultiTypesObjectPool<TPoolObjectDectiption,TObjectType> : MonoBehaviour where TPoolObjectDectiption : PoolObjectDescrtion<TObjectType>
{
    [SerializeField]
    protected Transform _poolParent;
    [SerializeField]
    protected List<TPoolObjectDectiption> _poolObjectDescrtions;
    protected Dictionary<TObjectType, IList<GameObject>> _pool;

    public virtual GameObject GetFromPool(TObjectType objectType, Vector3 position, Quaternion rotation, Transform parent)
    {
        if (_pool == null)
        {
            CreatePool();
        }

        var notActiveObject = _pool[objectType].FirstOrDefault(x => !x.activeInHierarchy);
        var objectPrefab = _poolObjectDescrtions.FirstOrDefault(x => x.ObjectName.Equals(objectType)).Prefab;

        if (notActiveObject == null)
        {
            notActiveObject = CreateObjectForPool(objectPrefab);
            AddToPool(objectType, notActiveObject);
        }

        notActiveObject.transform.parent = parent;
        notActiveObject.transform.position = position;
        notActiveObject.transform.rotation = rotation;
        notActiveObject.transform.localScale = objectPrefab.transform.localScale;
        notActiveObject.SetActive(true);

        return notActiveObject;
    }

    public virtual void ReleaseToPool(GameObject gameObject)
    {
        if(gameObject == null)
        {
            return;
        }

        gameObject.SetActive(false);
        gameObject.transform.parent = _poolParent;
        gameObject.transform.position = Vector3.zero;
        gameObject.transform.rotation = Quaternion.identity;
    }

    public virtual void ReturnAllToPool()
    {
        foreach (var poolObjectType in _pool.Keys)
        {
            foreach (var poolObject in _pool[poolObjectType])
            {
                ReleaseToPool(poolObject);
            }         
        }
    }

    protected virtual void CreatePool()
    {
        _pool = new Dictionary<TObjectType, IList<GameObject>>();

        foreach (var poolObject in _poolObjectDescrtions)
        {
            for (int i = 0; i < poolObject.PoolSize; i++)
            {
                var newObject = CreateObjectForPool(poolObject.Prefab, $"{poolObject.ObjectName.ToString()}-{i}");
                AddToPool(poolObject.ObjectName, newObject);
            }
        }
    }

    protected virtual GameObject CreateObjectForPool(GameObject prefab, string name = "object-")
    {
        var newObject = Instantiate(prefab, _poolParent);
        newObject.SetActive(false);

        newObject.name = name;

        return newObject;
    }

    protected virtual void AddToPool(TObjectType scene, GameObject @object)
    {
        if (_pool.ContainsKey(scene))
        {
            _pool[scene].Add(@object);
        }
        else
        {
            _pool.Add(scene, new List<GameObject> { @object });
        }
    }
}
