﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class TextureResizer
{
    public static Texture2D ResizeToTexture(this Texture2D source, int newWidth, int newHeight)
    {
        RenderTexture rt = new RenderTexture(newWidth, newHeight, 0);
        rt.filterMode = FilterMode.Point;
        rt.anisoLevel = 0;
        rt.antiAliasing = 1;
        rt.autoGenerateMips = false;
        rt.useMipMap = false;
        rt.format = RenderTextureFormat.ARGB32;

        Graphics.Blit(source, rt);

        Texture2D result = rt.ToTexture2D();

        return result;
    }

}

