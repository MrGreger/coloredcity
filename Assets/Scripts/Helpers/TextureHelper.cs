﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;

public static class TextureHelper
{
    public static Texture2D ToTexture2D(this RenderTexture renderTexture)
    {
        Stopwatch sw = new Stopwatch();

        sw.Start();

        RenderTexture lastRenderTexture = RenderTexture.active;

        Texture2D texture = CreateTexture2D(renderTexture.width, renderTexture.height);

        RenderTexture.active = renderTexture;
        texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture.Apply();

        RenderTexture.active = lastRenderTexture;

        sw.Stop();

        Debug.Log("reading: " + sw.ElapsedMilliseconds);

        sw.Reset();

        return texture;
    }

    public static Texture2D CreateTexture2D(int width, int height)
    {
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGBA32, 0, false);
        texture.filterMode = FilterMode.Point;
        return texture;
    }
}

