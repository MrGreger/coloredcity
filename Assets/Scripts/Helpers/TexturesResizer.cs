﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TexturesResizer : MonoBehaviour
{
    private void Start()
    {
        var painters = FindObjectsOfType<Painter>();

        var biggest = painters.FirstOrDefault(x => x.GetComponent<Collider>().bounds.size.magnitude == painters.Max(i => i.GetComponent<Collider>().bounds.size.magnitude));

        var painterMagnitudes = painters.Select(x =>
        {
            return new KeyValuePair<Painter, float>(x, x.GetComponent<Collider>().bounds.size.magnitude);
        });

        var biggestResolution = painterMagnitudes.FirstOrDefault(x => x.Key == biggest).Value;

        foreach (var item in painterMagnitudes)
        {
            var ratio = (item.Value / biggestResolution);

            item.Key.SetResolution((int)(biggest.Resolution * ratio));
        }
    }
}
