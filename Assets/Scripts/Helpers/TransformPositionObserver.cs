﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TransformPositionObserver : MonoBehaviour
{
    public UnityEventVector3 PositionChanged;

    [SerializeField]
    [Range(0f, 100f)]
    private float _minDelta;
    private Vector3 _lastPosition;

    private void Update()
    {
        if (_lastPosition != transform.position)
        {
            if (Vector3.Distance(_lastPosition, transform.position) < _minDelta)
            {
                return;
            }

            PositionChanged?.Invoke(transform.position);
            _lastPosition = transform.position;
        }
    }
}
