﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    private const string SPEED = "Speed";

    [SerializeField]
    private float _userSpeed = 10f;

    private IInputHandler _inputHandler;
    private Animator _animator;
    private Rigidbody _rigidbody;

    private Vector2 _userInput = Vector2.zero;

    [Inject]
    public void Constructor(IInputHandler inputHandler)
    {
        _inputHandler = inputHandler;
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        _userInput = _inputHandler.GetInput();
        _animator.SetFloat(SPEED, _userInput.magnitude);
    }

    public void FixedUpdate()
    {
        var direction = -new Vector3(_userInput.x, 0, _userInput.y);

        if (direction.sqrMagnitude == 0)
        {
            _rigidbody.velocity = Vector3.zero;
            return;
        }

        _rigidbody.velocity = Vector3.zero;

        RoatatePlayer(direction);

        MovePlayer(direction);
    }

    private void MovePlayer(Vector3 direction)
    {
        var newPosition = transform.position + direction * _userSpeed * Time.deltaTime;

        _rigidbody.MovePosition(newPosition);
    }

    private void RoatatePlayer(Vector3 direction)
    {
        var angle = Quaternion.LookRotation(direction, transform.up);

        _rigidbody.MoveRotation(angle);
    }
}
