﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class HumanMover : MonoBehaviour
{
    private HumansDispatcher _humansDispatcher;
    private IInputHandler _inputHandler;

    [Inject]
    private void Constructor(HumansDispatcher humansDispatcher, IInputHandler inputHandler)
    {
        _humansDispatcher = humansDispatcher;
        _inputHandler = inputHandler;
    }

    private void Update()
    {
        var player = _humansDispatcher.GetPlayer();

        var userInput = _inputHandler.GetInput();

        if (player == null)
        {
            return;
        }

        var direction = -new Vector3(userInput.x, 0, userInput.y) * 10;

        if (direction.sqrMagnitude != 0)
        {
            player.HumanAI.SetTarget(player.Transform.position + direction);
        }
        else
        {
            player.HumanAI.SetTarget(null);
        }
    }
}
