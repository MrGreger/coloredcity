﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New behaviour", menuName = "Behaviour/Create new")]
public class Behaviour : ScriptableObject
{
    [SerializeField]
    private List<ActionBase> _actions;
    [SerializeField]
    private List<Transition> _transitions;

    public virtual void Behave(IHumanAI humanAI)
    {
        DoActions(humanAI);
        CheckTransitions(humanAI);
    }

    protected virtual void DoActions(IHumanAI humanAI)
    {
        for (int i = 0; i < _actions.Count; i++)
        {
            _actions[i].Act(humanAI);
        }
    }

    protected virtual void CheckTransitions(IHumanAI humanAI)
    {
        for (int i = 0; i < _transitions.Count; i++)
        {
            var nextBehaviour = _transitions[i].GetNextBehaviour(humanAI);

            if (nextBehaviour != null)
            {
                humanAI.SwitchBehaviour(nextBehaviour);
            }
        }
    }
}

