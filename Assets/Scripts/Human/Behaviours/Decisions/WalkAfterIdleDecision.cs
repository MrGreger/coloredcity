﻿
using UnityEngine;

[CreateAssetMenu(fileName = "WalkAfterIdle", menuName = "Behaviour/Decisions/WalkAfterIdle")]
public class WalkAfterIdleDecision : DecisionBase
{
    [SerializeField]
    private float _minIdleTime = 1;
    [SerializeField]
    private float _maxIdleTime = 3;

    public override bool Decide(IHumanAI humanAI)
    {
        if(humanAI.Target != null && humanAI.Human.Squad != null)
        {
            return true;
        }

        for (int i = 0; i < humanAI.Timers.Count; i++)
        {
            AITimer t = humanAI.Timers[i];
            if (t.Name == nameof(IdleTimer))
            {
                if (!t.Started)
                {
                    t.Start(Random.Range(_minIdleTime, _maxIdleTime));
                }
                else
                {
                    if (t.TimerTicked)
                    {
                        t.Reset();
                        return true;
                    }
                }
            }
        }

        return false;
    }
}

