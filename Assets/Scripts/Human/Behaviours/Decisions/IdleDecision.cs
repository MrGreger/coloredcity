﻿using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "Go idle", menuName = "Behaviour/Decisions/Go idle")]
public class IdleDecision : DecisionBase
{
    public override bool Decide(IHumanAI humanAI)
    {
        if (humanAI.NavMeshAgent.pathPending || !humanAI.NavMeshAgent.isOnNavMesh)
        {
            return false;
        }

        return  humanAI.NavMeshAgent.remainingDistance <= humanAI.NavMeshAgent.stoppingDistance;
    }
}

