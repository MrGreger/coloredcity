﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "Select random point", menuName = "Behaviour/Decisions/Select random move point")]
public class SelectRandomPointDecision : DecisionBase
{
    public override bool Decide(IHumanAI humanAI)
    {
        if (humanAI.Human.PlayerControlled)
        {
            return false;
        }

        return humanAI.Human.Squad == null || humanAI.Human.IsLeader;
    }
}

