﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
public class Transition
{
    [SerializeField]
    private DecisionBase _decision;
    [SerializeField]
    protected Behaviour _trueBehaviour;
    [SerializeField]
    protected Behaviour _falseBehaviour;
    [SerializeField]
    protected Behaviour _defaultBehaviour;

    public virtual Behaviour GetNextBehaviour(IHumanAI humanAI)
    {
        if(_decision == null)
        {
            return _defaultBehaviour;
        }

        return _decision.Decide(humanAI) ? _trueBehaviour : _falseBehaviour;
    }
}

