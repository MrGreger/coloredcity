﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "Select random move point", menuName = "Behaviour/Actions/Select random move point")]
public class SelectRandomPoint : ActionBase
{
    [SerializeField]
    private float _randomPointRadius;

    public override void Act(IHumanAI humanAI)
    {
        humanAI.SetTarget(RandomNavmeshLocation(humanAI, _randomPointRadius));
    }

    private Vector3 RandomNavmeshLocation(IHumanAI human, float radius)
    {
        // Get Random Point inside Sphere which position is center, radius is maxDistance
        Vector3 randomPos = Random.insideUnitSphere * radius + human.GameObject.transform.position;

        NavMeshHit hit; // NavMesh Sampling Info Container

        // from randomPos find a nearest point on NavMesh surface in range of maxDistance
        while (!NavMesh.SamplePosition(randomPos, out hit, Random.Range(0f, radius), 1))
        {
        }

        return hit.position;
    }
}

