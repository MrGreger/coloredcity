﻿using UnityEngine;

public abstract class ActionBase : ScriptableObject
{
    public abstract void Act(IHumanAI humanAI);
}

