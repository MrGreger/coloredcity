﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Shoot", menuName = "Behaviour/Actions/Shoot")]
public class ShootAction : ActionBase
{
    [SerializeField]
    private bool _noTargetsShoot = false;

    public override void Act(IHumanAI humanAI)
    {
        if(humanAI.Human.Squad == null)
        {
            return;
        }

        for (int i = 0; i < humanAI.Timers.Count; i++)
        {
            AITimer t = humanAI.Timers[i];
            if (t.Name == nameof(CheckTargetsTimer))
            {
                if (!t.Started)
                {
                    t.Start(1.4f);
                }
                else
                {
                    if (t.TimerTicked)
                    {
                        humanAI.Human.SelectTarget();
                        t.Reset();                       
                    }
                }
            }
        }

        var target = humanAI.Human.Target;

        if (target == null)
        {
            if (_noTargetsShoot)// Should we shoot when there is no anyone?
            {
                humanAI.Human.Weapon.Shoot();
            }
        }
        else
        {
            var shootDirection = (target.Transform.position - humanAI.GameObject.transform.position);

            if (humanAI.HumanIK != null)
            {
                //humanAI.HumanIK.EnableIk(target.Transform);
            }

            humanAI.Human.Weapon.Shoot(shootDirection);

            humanAI.GameObject.transform.LookAt(target.Transform.position, humanAI.GameObject.transform.up);
        }
    }
}

