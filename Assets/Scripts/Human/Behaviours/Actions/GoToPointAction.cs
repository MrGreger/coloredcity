﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "GoToPointAction", menuName = "Behaviour/Actions/GoToPointAction")]
public class GoToPointAction : ActionBase
{
    protected const string SPEED = "Speed";

    public override void Act(IHumanAI humanAI)
    {
        if (humanAI.Target.HasValue && humanAI.NavMeshAgent.isOnNavMesh)
        {
            humanAI.NavMeshAgent.SetDestination(humanAI.Target.Value);
        }
        else if (humanAI.Target == null && humanAI.NavMeshAgent.isOnNavMesh)
        {
            humanAI.NavMeshAgent.ResetPath();
        }

        UpdateAnimation(humanAI);
    }

    private void UpdateAnimation(IHumanAI humanAI)
    {
        if (humanAI.NavMeshAgent.velocity.sqrMagnitude > 0.3f)
        {
            humanAI.Animator.SetFloat(SPEED, humanAI.NavMeshAgent.velocity.normalized.magnitude);
        }
        else
        {
            humanAI.Animator.SetFloat(SPEED, 0);
        }
    }
}

