﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "Stand idle", menuName = "Behaviour/Actions/Stand idle")]
public class IdleAction : ActionBase
{
    protected const string SPEED = "Speed";

    public override void Act(IHumanAI humanAI)
    {
        humanAI.Animator.SetFloat(SPEED, 0);
    }
}

