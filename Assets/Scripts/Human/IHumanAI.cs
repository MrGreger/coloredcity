﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

public interface IHumanAI
{
    IHuman Human { get; }
    Vector3? Target { get; }
    void SetTarget(Vector3? target);
    GameObject GameObject { get; }
    NavMeshAgent NavMeshAgent { get; }
    Animator Animator { get; }
    HumansDispatcher HumansDispatcher { get; }
    HumanIK HumanIK { get; }
    void SwitchBehaviour(Behaviour behaviour);
    IList<AITimer> Timers { get; }
}

