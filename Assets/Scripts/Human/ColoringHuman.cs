﻿using Es.InkPainter;
using System.Linq;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(IHuman))]
public class ColoringHuman : MonoBehaviour
{
    private IHuman _colorableHuman;

    [SerializeField]
    private Brush _brush;

    [Inject]
    private void Constructor()
    {
        _colorableHuman = GetComponent<IHuman>();

        _colorableHuman.ColorChanged += UpdateColor;
    }

    private RaycastHit[] _hitsBuffer = new RaycastHit[5];

    public void Paint()
    {
        if (_colorableHuman.Squad == null)
        {
            return;
        }

        var ray = new Ray(transform.position + Vector3.up, -transform.up);

        var hits = Physics.RaycastNonAlloc(ray, _hitsBuffer, 3f);

        for (int i = 0; i < hits; i++)
        {
            if (_hitsBuffer[i].transform.TryGetComponent<IColorableSurface>(out var c))
            {
                if (_colorableHuman.SquadColor.HasValue)
                {
                    c?.Draw(_hitsBuffer[i], _brush);
                }
            }
        }
    }

    private void UpdateColor()
    {
        if (_colorableHuman.SquadColor == null)
        {
            return;
        }

        _brush.Color = _colorableHuman.SquadColor.Value;
    }

    private void OnDestroy()
    {
        _colorableHuman.ColorChanged -= UpdateColor;
    }
}

