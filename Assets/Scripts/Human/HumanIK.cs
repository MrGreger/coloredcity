﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HumanIK : MonoBehaviour
{
    [SerializeField]
    private Transform _leftArm;
    [SerializeField]
    private Transform _rightArm;
    [SerializeField]
    private Transform _head;

    private Animator _animator;

    public bool IKEnabled = false;

    public Transform _lookTarget;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void EnableIk(Transform lookTarget)
    {
        IKEnabled = true;
        _lookTarget = lookTarget;
    }

    public void DisableIk()
    {
        IKEnabled = false;
        _lookTarget = null;
    }

    private void OnAnimatorIK(int layerIndex)
    {
        //if (!IKEnabled || _lookTarget == null)
        //{
        //    return;
        //}
        //_animator.SetLookAtWeight(1);
        //_animator.SetLookAtPosition(_lookTarget.position);
        //_head.LookAt(_lookTarget);

        //_animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
        //_animator.SetIKPosition(AvatarIKGoal.RightHand, _rightArm.position);

        //_animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 10);
        //_animator.SetIKPosition(AvatarIKGoal.LeftHand, _leftArm.position);

        //_leftArm.SetParent(_head);
        //_rightArm.SetParent(_head);
    }
}
