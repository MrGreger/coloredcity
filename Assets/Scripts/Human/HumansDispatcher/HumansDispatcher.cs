﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;
using Zenject;

public class HumansDispatcher : MonoBehaviour
{
    private List<IHuman> _humans;

    private IHuman _player;
    public ReactiveCollection<Squad> Squads { get; private set; }

    public event Action<Squad> SquadDied;


    private void Start()
    {
        _humans = new List<IHuman>();

        _humans.AddRange(FindObjectsOfType<MonoBehaviour>().Where(x => x is IHuman).Cast<IHuman>());

        Squads = new ReactiveCollection<Squad>(new HashSet<Squad>());

        foreach (var human in _humans)
        {
            Squads.Add(human.Squad);
        }

        foreach (var squad in Squads)
        {
            squad.MemverRemoved += Squad_MemverRemoved;
        }
    }

    private void OnDisable()
    {
        foreach (var squad in Squads)
        {
            squad.MemverRemoved -= Squad_MemverRemoved;
        }
    }

    private void Squad_MemverRemoved(Squad squad, IHuman obj)
    {
        if(squad.SquadStrength == 0 && squad.SquadLeader == null)
        {
            Squads.Remove(squad);
            SquadDied?.Invoke(squad);
        }
    }

    public void AddToTracking(IHuman human)
    {
        if (_humans.Contains(human))
        {
            return;
        }

        _humans.Add(human);
    }

    public IEnumerable<IHuman> GetHumans()
    {
        return _humans;
    }

    public IEnumerable<Color32> GetColors()
    {
        HashSet<Color32> colors = new HashSet<Color32>();

        foreach (var human in _humans)
        {
            if (human.SquadColor.HasValue)
            {
                colors.Add(human.SquadColor.Value);
            }
        }

        return colors;
    }

    public IHuman GetPlayer()
    {
        if (_player != null)
        {
            if (_player.IsLeader && _player.PlayerControlled)
            {
                return _player;
            }
            else
            {
                _player = null;
            }
        }

        foreach (var human in _humans)
        {
            if (human.IsLeader && human.PlayerControlled)
            {
                _player = human;
            }
        }

        return _player;
    }

    public IList<IHuman> GetLeaders()
    {
        var result = new List<IHuman>();

        foreach (var human in _humans)
        {
            if (human.IsLeader)
            {
                result.Add(human);
            }
        }

        return result;
    }
}

