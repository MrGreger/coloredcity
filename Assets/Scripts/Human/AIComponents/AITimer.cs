﻿using UnityEngine;

public abstract class AITimer
{
    public abstract string Name { get; }

    public bool Started { get; protected set; } = false;

    public virtual bool TimerTicked => _timeElapsed >= _timerTime;

    protected float _timeElapsed;
    protected float _timerTime;

    public virtual void Start(float time)
    {
        Started = true;
        _timeElapsed = 0;
        _timerTime = time;
    }

    public virtual void Tick()
    {
        if (Started)
        {
            _timeElapsed += Time.deltaTime;
        }
    }

    public virtual void Reset()
    {
        Started = false;
        _timeElapsed = 0;
    }
}
