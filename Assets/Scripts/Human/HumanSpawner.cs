﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

public class HumanSpawner : MonoBehaviour
{
    [SerializeField]
    [Range(1, 100)]
    private int _spawnCount = 10;
    [SerializeField]
    [Range(1, 100)]
    private float _areaRange = 10;

    private Human.HumanFactory _humanFactory;

    [Inject]
    private void Constructor(Human.HumanFactory humanFactory)
    {
        _humanFactory = humanFactory;
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        SpawnHumans();
    }

    private void SpawnHumans()
    {
        for (int i = 0; i < _spawnCount; i++)
        {
            var randomPos = GetRandomPoint(transform.position, _areaRange);

            var human = _humanFactory.Create();
            human.transform.position = randomPos;

            human.transform.rotation = Random.rotation;
            var rot = human.transform.rotation;

            rot.eulerAngles = new Vector3(0, rot.eulerAngles.y, 0);

            human.transform.rotation = rot;

            human.transform.parent = transform;
        }
    }

    private Vector3 GetRandomPoint(Vector3 squadRandomPosition, float radius)
    {
        var humanRandomPoint = Random.insideUnitCircle * radius;
        var position = squadRandomPosition + new Vector3(humanRandomPoint.x, 0, humanRandomPoint.y);

        if (NavMesh.SamplePosition(position, out var navHit, 1f, NavMesh.AllAreas))
        {
            return navHit.position;
        }

        Debug.LogError("Failed to sample position");

        return position;
    }

    private void OnDrawGizmos()
    {
        var oldColor = Gizmos.color;

        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(transform.position, _areaRange / 2);

        Gizmos.color = oldColor;
    }

}
