﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class HumansFactory : IFactory<Object, Human>
{
    readonly DiContainer _container;

    public HumansFactory(DiContainer container)
    {
        _container = container;
    }

    public Human Create(Object prefab)
    {
        return _container.InstantiatePrefabForComponent<Human>(prefab);
    }
}