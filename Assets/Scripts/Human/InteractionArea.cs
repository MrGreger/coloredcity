﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InteractionArea : MonoBehaviour
{
    public event System.Action<IHuman> CollidedWithHuman;

    private IHuman _handler;

    [SerializeField]
    private bool _enabled;

    [Inject]
    private void Constructor()
    {
        _handler = GetComponentInParent<IHuman>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_enabled)
        {
            return;
        }

        if (other.transform.parent.TryGetComponent<IHuman>(out var human))
        {
            if (_handler == human)
            {
                return;
            }

            CollidedWithHuman?.Invoke(human);
        }
        else if (other.TryGetComponent<IHuman>(out var h))
        {
            if (_handler == h)
            {
                return;
            }

            CollidedWithHuman?.Invoke(h);
        }
    }
}
