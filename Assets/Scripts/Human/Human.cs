﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Collider))]
public class Human : MonoBehaviour, IHuman
{

    [SerializeField]
    private Color _squadColor;
    [SerializeField]
    private bool _isLeader;
    private List<IHuman> _availableTargets;
    private InteractionArea _interactionArea;
    [SerializeField]
    private bool _playerControlled;

    [SerializeField]
    protected Squad _squad;

    public IWeapon Weapon { get; private set; }
    public Squad Squad => _squad;
    public Color? SquadColor { get => _squadColor; private set => _squadColor = value.Value; }
    public bool IsLeader { get => _isLeader; private set => _isLeader = value; }
    public Transform Transform => transform;
    public IList<IHuman> AvailableTargets => _availableTargets.AsReadOnly();
    public event Action ColorChanged;
    public event Action EnemySpoted;

    public Collider Collider { get; protected set; }

    public bool PlayerControlled { get => _playerControlled; private set { _playerControlled = value; } }
    public IHuman Target { get; private set; }

    public IHumanAI HumanAI { get; private set; }


    private Collider[] _collidersBuffer = new Collider[100];

    [Inject]
    private void Constructor()
    {
        Weapon = GetComponentInChildren<Marker>();
        Weapon.SetOwner(this);
        _interactionArea = GetComponentInChildren<InteractionArea>();

        HumanAI = GetComponent<IHumanAI>();

        _availableTargets = new List<IHuman>();

        _interactionArea.CollidedWithHuman += OnCollidedWithHuman;

        Collider = GetComponent<Collider>();
    }

    private int GetHumanCollidersInAttackRange()
    {
        for (int i = 0; i < _collidersBuffer.Length; i++)
        {
            _collidersBuffer[i] = null;
        }

        return Physics.OverlapSphereNonAlloc(transform.position, Weapon.AttackRange, _collidersBuffer, LayerMask.GetMask("HumanColliders"));
    }

    private void OnSquadMemverAdded(Squad squad, IHuman obj)
    {
        if (Target == null)
        {
            return;
        }

        if (Target == obj || Target.Squad == Squad)
        {
            Target = null;
        }
    }

    private void OnCollidedWithHuman(IHuman human)
    {
        // If neutral, ignore collision
        if (Squad == null)
        {
            return;
        }

        // Human already has squad.
        // Now you can assign him to our squad
        // Only by shooting
        if (human.Squad != null)
        {
            return;
        }

        // Assign to our squad human
        if (IsLeader || Squad.SquadLeader != null)
        {
            Squad.HumanHitted(human);
        }
    }

    private void Awake()
    {
        if (_isLeader)
        {
            SquadColor = _squadColor;
            _squad = new Squad(this, new RandomSquadFormatter(4), new List<IHuman>());
            SetColor(_squadColor);

            _squad.MemverAdded += OnSquadMemverAdded;
        }
    }

    public void AssignToSquad(Squad squad)
    {
        if (_squad != null)
        {
            _squad.MemverAdded -= OnSquadMemverAdded;
        }

        _squad = squad;

        if (_squad != null)
        {
            _squad.MemverAdded += OnSquadMemverAdded;

            SetColor(squad.SquadLeader?.SquadColor ?? Color.white);
        }
    }

    public void OnMoved()
    {
        if (!IsLeader)
        {
            return;
        }

        Squad?.RelocateSquad(transform.position);
    }


    public void SetLeader(bool isLeader)
    {
        IsLeader = isLeader;
    }

    public void SetColor(Color color)
    {
        color.a = .97f;

        SquadColor = color;
        ColorChanged?.Invoke();
        GetComponentInChildren<SkinnedMeshRenderer>().material.color = color;
    }

    private void OnDisable()
    {
        _interactionArea.CollidedWithHuman -= OnCollidedWithHuman;
        if (_squad != null)
        {
            _squad.MemverAdded -= OnSquadMemverAdded;
        }
    }

    public void SelectTarget()
    {
        if (Squad == null)
        {
            return;
        }

        if (Target != null && Target.Squad != Squad)
        {
            var distance = Vector3.Distance(Target.Transform.position, transform.position);
            if (distance <= Weapon.AttackRange)
            {
                return;
            }
        }

        Target = null;

        var length = GetHumanCollidersInAttackRange();

        for (int i = 0; i < length; i++)
        {
            if (_collidersBuffer[i].transform.parent.TryGetComponent<IHuman>(out var target))
            {
                if (target.Squad == Squad)
                {
                    continue;
                }

                Target = target;
                return;
            }
        }
    }

    public void SetPlayerControlled(bool playerControlled)
    {
        if (PlayerControlled && IsLeader && !playerControlled)
        {
            Debug.LogError("Can not unset player controlled when it leader.");
            return;
        }

        PlayerControlled = playerControlled;
    }

    public class HumanFactory : PlaceholderFactory<Human>
    {
        private HumansDispatcher _humansDispatcher;

        [Inject]
        private void Constructor(HumansDispatcher humansDispatcher)
        {
            _humansDispatcher = humansDispatcher;
        }

        public override Human Create()
        {
            var human = base.Create();

            _humansDispatcher.AddToTracking(human);

            return human;
        }
    }
}
