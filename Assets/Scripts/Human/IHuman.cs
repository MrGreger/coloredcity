﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public interface IHuman
{
    Transform Transform { get; }
    Squad Squad { get; }
    Color? SquadColor { get; }
    void AssignToSquad(Squad squad);
    void SetLeader(bool isLeader);
    bool IsLeader { get; }
    Collider Collider { get; }
    IHumanAI HumanAI { get; }

    bool PlayerControlled { get; }
    void SetPlayerControlled(bool playerControlled);

    IHuman Target { get; }
    void SelectTarget();

    IList<IHuman> AvailableTargets { get; }

    IWeapon Weapon { get; }

    event Action ColorChanged;
    event Action EnemySpoted;
}

