﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(IHuman))]
[RequireComponent(typeof(HumanIK))]
public class HumanAIController : MonoBehaviour, IHumanAI
{
    public NavMeshAgent NavMeshAgent { get; private set; }
    public Animator Animator { get; private set; }
    public GameObject GameObject => gameObject;

    public Vector3? Target { get; private set; }

    public IHuman Human { get; private set; }

    public HumansDispatcher HumansDispatcher { get; private set; }

    public HumanIK HumanIK { get; private set; }

    private List<AITimer> _timers;
    public IList<AITimer> Timers => _timers;

    [SerializeField]
    private Behaviour _behaviour;

    private static WaitForSeconds _waitForInit = new WaitForSeconds(0.1f);

    [Inject]
    protected void Constructor(HumansDispatcher humansDispatcher)
    {
        NavMeshAgent = GetComponent<NavMeshAgent>();
        Animator = GetComponent<Animator>();
        Human = GetComponent<IHuman>();
        HumanIK = GetComponent<HumanIK>();

        HumansDispatcher = humansDispatcher;

        _timers = new List<AITimer>
        {
            new IdleTimer(),
            new CheckTargetsTimer()
        };

    }

    private void Start()
    {
        StartCoroutine(ReInitNavMeshAgent());
    }

    private IEnumerator ReInitNavMeshAgent()
    {
        yield return _waitForInit;

        NavMeshAgent.enabled = false;
        NavMeshAgent.enabled = true;
    }

    protected void Update()
    {
        _behaviour?.Behave(this);
        TickTimers();
    }

    private void TickTimers()
    {
        for (int i = 0; i < _timers.Count; i++)
        {
            _timers[i].Tick();
        }
    }

    public void SetTarget(Vector3? target)
    {
        Target = target;
    }

    public void SwitchBehaviour(Behaviour behaviour)
    {
        if (behaviour != null)
        {
            _behaviour = behaviour;
        }
    }

    private void OnDrawGizmos()
    {
        if (Target != null)
        {
            var lastColor = Gizmos.color;
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(Target.Value, 0.1f);
            Gizmos.DrawLine(Target.Value, transform.position);
            Gizmos.color = lastColor;
        }

        if (Human != null && Human.Target != null)
        {
            var lastColor = Gizmos.color;
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(Human.Target.Transform.position, 0.1f);
            Gizmos.DrawLine(Human.Target.Transform.position, transform.position);
            Gizmos.color = lastColor;
        }
    }
}

