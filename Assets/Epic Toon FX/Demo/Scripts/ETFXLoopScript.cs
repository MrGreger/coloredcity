﻿using UnityEngine;
using System.Collections;

namespace EpicToonFX
{
	public class ETFXLoopScript : MonoBehaviour
	{
		public GameObject ChosenEffect;
		public float LoopTimeLimit = 2.0f;

        private static WaitForSeconds _loopWait;

		[Header("Spawn without")]
	
		public bool SpawnWithoutLight = true;
		public bool SpawnWithoutSound = true;

		void Start ()
		{
            if (_loopWait == null)
            {
                _loopWait = new WaitForSeconds(LoopTimeLimit);
            }

            PlayEffect();
		}

		public void PlayEffect()
		{
			StartCoroutine("EffectLoop");
		}

		IEnumerator EffectLoop()
		{
			GameObject effectPlayer = (GameObject) Instantiate(ChosenEffect, transform.position, transform.rotation);
		
			if(SpawnWithoutLight = true && effectPlayer.GetComponent<Light>())
			{
				effectPlayer.GetComponent<Light>().enabled = false;
				//Destroy(gameObject.GetComponent<Light>());

			}
		
			if(SpawnWithoutSound = true && effectPlayer.GetComponent<AudioSource>())
			{
				effectPlayer.GetComponent<AudioSource>().enabled = false;
				//Destroy(gameObject.GetComponent<AudioSource>());
			}
				
			yield return _loopWait;

			Destroy (effectPlayer);
			PlayEffect();
		}
	}
}