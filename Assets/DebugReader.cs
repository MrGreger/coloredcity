using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugReader : MonoBehaviour
{

    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        Application.logMessageReceived += OnLog;
    }

    private void OnLog(string condition, string stackTrace, LogType type)
    {
        if(text == null)
        {
            return;
        }

        text.text += $"{condition} {Environment.NewLine}";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
